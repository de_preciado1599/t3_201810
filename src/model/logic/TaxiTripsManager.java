package model.logic;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Lista;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {


	private Stack<Service> serviciosStack;
	private Queue<Service> serviciosQueue;
	private int contador;

	public void loadServices(String serviceFile, String pTaxiId) {
		serviciosQueue= new Queue<Service>();
		serviciosStack= new Stack<Service>();
		contador=0;
		JsonParser parser = new JsonParser();
		Long tiempoinicial = System.currentTimeMillis();
		try {

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				String taxiId= null;
				if ( obj.get("taxi_id") != null ){
					taxiId= obj.get("taxi_id").getAsString();
				}
				if(taxiId.equals(pTaxiId)){
					String company = "NaN";
					if ( obj.get("company") != null ){
						company = obj.get("company").getAsString();
					}

					double dropoff_census_tract = 0;
					if ( obj.get("dropoff_census_tract") != null ){
						dropoff_census_tract = obj.get("dropoff_census_tract").getAsDouble();
					}

					double dropoff_centroid_latitude = 0;
					if ( obj.get("dropoff_centroid_latitude") != null ){
						dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsDouble();
					}

					double[] dropoff_centroid_location = null;
					JsonObject centroidLocationObj = null;
					if ( obj.get("dropoff_centroid_location") != null ){
						dropoff_centroid_location = new double[2];
						centroidLocationObj = obj.get("dropoff_centroid_location").getAsJsonObject();
						JsonArray dropoff_localization_arr = centroidLocationObj.get("coordinates").getAsJsonArray();
						dropoff_centroid_location[0] = dropoff_localization_arr.get(0).getAsDouble();
						dropoff_centroid_location[1] = dropoff_localization_arr.get(1).getAsDouble();
					}

					double dropoff_centroid_longitude= 0;
					if ( obj.get("dropoff_centroid_longitude") != null ){
						dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsDouble();
					}

					double dropoff_community_area= 0;
					if ( obj.get("dropoff_community_area") != null ){
						dropoff_community_area= obj.get("dropoff_community_area").getAsDouble();
					}

					double extras= 0;
					if ( obj.get("extras") != null ){
						extras= obj.get("extras").getAsDouble();
					}

					double fare= 0;
					if ( obj.get("fare") != null ){
						fare= obj.get("fare").getAsDouble();
					}

					String payment_type= null;
					if ( obj.get("payment_type") != null ){
						payment_type= obj.get("payment_type").getAsString();
					}

					double pickup_census_tract= 0;
					if ( obj.get("pickup_census_tract") != null ){
						pickup_census_tract	= obj.get("pickup_census_tract").getAsDouble();
					}
					double pickup_centroid_latitude= 0;
					if ( obj.get("pickup_centroid_latitude") != null ){
						pickup_centroid_latitude= obj.get("pickup_centroid_latitude").getAsDouble();
					}


					double[] pickup_centroid_location = null;
					JsonObject pickup_centroid_locationObj = null;
					if ( obj.get("pickup_centroid_location") != null ){
						pickup_centroid_location = new double[2];
						pickup_centroid_locationObj = obj.get("pickup_centroid_location").getAsJsonObject();
						JsonArray pickup_centroid_location_arr = pickup_centroid_locationObj.get("coordinates").getAsJsonArray();
						pickup_centroid_location[0] = pickup_centroid_location_arr.get(0).getAsDouble();
						pickup_centroid_location[1] = pickup_centroid_location_arr.get(1).getAsDouble();
					}

					double pickup_centroid_longitude = 0;
					if ( obj.get("pickup_centroid_longitude") != null ){
						pickup_centroid_longitude	= obj.get("pickup_centroid_longitude").getAsDouble();
					}


					double pickup_community_area= 0;
					if ( obj.get("pickup_community_area") != null ){
						pickup_community_area= obj.get("pickup_community_area").getAsDouble();
					}

					String taxi_id= null;
					if ( obj.get("taxi_id") != null ){
						taxi_id= obj.get("taxi_id").getAsString();
					}


					double tips= 0;
					if ( obj.get("tips") != null ){
						tips= obj.get("tips").getAsDouble();
					}


					double tolls= 0;
					if ( obj.get("tolls") != null ){
						tolls= obj.get("tolls").getAsDouble();
					}


					String trip_end_timestamp= null;
					if ( obj.get("trip_end_timestamp") != null ){
						trip_end_timestamp= obj.get("trip_end_timestamp").getAsString();
					}

					String trip_id= null;
					if ( obj.get("trip_id") != null ){
						trip_id= obj.get("trip_id").getAsString();
					}

					double trip_miles= 0;
					if ( obj.get("trip_miles") != null ){
						trip_miles	= obj.get("trip_miles").getAsDouble();
					}

					double trip_seconds= 0;
					if ( obj.get("trip_seconds") != null ){
						trip_seconds= obj.get("trip_seconds").getAsDouble();
					}

					String String_trip_start_timestamp= "NaN";
					Calendar trip_start_timestamp=null;
					if ( obj.get("trip_start_timestamp") != null ){
						String_trip_start_timestamp= obj.get("trip_start_timestamp").getAsString();
					}
					String[] formato = String_trip_start_timestamp.split("T");
					String[] a�oMesDia = formato[0].split("-");
					int a�o = Integer.parseInt(a�oMesDia[0]);
					int mes = Integer.parseInt(a�oMesDia[1]);
					int dia = Integer.parseInt(a�oMesDia[2]);
					String[] horasMinutosSegundos = formato[1].split(":");
					int horas = Integer.parseInt(horasMinutosSegundos[0]);
					int minutos = Integer.parseInt(horasMinutosSegundos[1]);
					int segundos = (int) Double.parseDouble(horasMinutosSegundos[2]);
					trip_start_timestamp= new GregorianCalendar(a�o, (mes+1), dia, horas, minutos, segundos);

					double trip_total= 0;
					if ( obj.get("trip_total") != null ){
						trip_total= obj.get("trip_total").getAsDouble();
					}
					// Se crea un nuevo servicio y se agrega a la lista de servicios
					Service current = new Service(company, dropoff_census_tract, dropoff_centroid_latitude, dropoff_centroid_location, dropoff_centroid_longitude, dropoff_community_area, extras, fare, payment_type, pickup_census_tract, pickup_centroid_latitude, pickup_centroid_location, pickup_centroid_longitude, pickup_community_area, taxi_id, tips, tolls, trip_end_timestamp, trip_id, trip_miles, trip_seconds, trip_start_timestamp, trip_total);
					serviciosQueue.enqueue(current);
					serviciosStack.push(current);
					contador++;
				}
			}
			System.out.println("La cantidad de servicios son " +contador);
			Long tiempototal = (System.currentTimeMillis()-tiempoinicial)/1000;
			System.out.println("El tiempo total usado en la carga de los archivos fue de " +tiempototal + " Segundos");
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}

	@Override
	/**
	 * @author Sebastian Mujica
	 */
	public int [] servicesInInverseOrder() 
	{
		// TODO Auto-generated method stub
		int [] resultado = new int[2];

		Service actual = serviciosStack.pop();
		Service siguiente = serviciosStack.pop();
		while(siguiente!=null)
		{
			if(actual.getTrip_start_timestamp().after(siguiente.getTrip_start_timestamp())){
				resultado[0]++;
				if(serviciosStack.isEmpty())
				{
					resultado[0]++;
				}
			}
			else{
				resultado[1]++;
			}
			actual = siguiente;
			siguiente = serviciosStack.pop();
		}
		return resultado;
	}

	/**
	 * @author Daniel Preciado
	 */
	public int [] servicesInOrder() 
	{
		// TODO Auto-generated method stub
		int [] retorno = new int [2];
		Service act = serviciosQueue.dequeue();
		Service sgt = serviciosQueue.dequeue();
		boolean termino = false;
		if(act.getTrip_start_timestamp().before(sgt.getTrip_start_timestamp())) 
		{
			retorno [0]++;
		}
		else
		{
			retorno[1]++;
		}
		//comprobacion de que esta analizando servicios distintos
		System.out.println("id : "+act.getTrip_miles() +"  #primero externo : ");
		for(int i = 0; !termino ; i++) 
		{
			if(act.getTrip_start_timestamp().before(sgt.getTrip_start_timestamp())) 
			{
				retorno [0]++;
			}
			else 
			{
				retorno[1]++;				
			}

			act = sgt;
			sgt = serviciosQueue.dequeue();
			if(sgt == null ) 
			{
				termino = true;
			}
			//comprobacion de que esta analizando servicios distintos 
			System.out.println("id : "+act.getTrip_miles() +"  #iteracion : " + i);
		}
		return retorno;
	}


}
