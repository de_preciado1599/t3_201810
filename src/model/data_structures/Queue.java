package model.data_structures;

/**
 * clase que representa una cola
 * @author de.preciado
 * @param <T> representa el
 * objeto T que va  a ser encolado
 */

public class Queue <T extends Comparable<T>>  implements IQueue<T>{

	//_________________________________________________________
	//Atributos
	//_________________________________________________________
	
	/**
	 * Representa el primer nodo de la cola.
	 */
	private Node cabeza; 
	
	/**
	 * clase que representa un nodo
	 * que contiene el elemento T
	 * @author de.preciado
	 * clase auxiliar
	 */
	private class Node 
	{
		//__________________________________
		//atributos
		//__________________________________

		/**
		 * Representa el Elemento actual
		 * del nodo.
		 */
		private T elemento;

		/**
		 * Representa el siguiende Nodo 
		 * al  actual.
		 */
		private Node siguiente;
		
		//___________________________________
		//Metodos
		//___________________________________
		 
		/**
		 * construye un nuevo nodo con un  
		 * elemento t que recibe por parametro
		 * @param t
		 */
		public Node(T t)
		{
			elemento = t;
		}

		/**
		 * Da el elemento del nodo actual
		 * @return elemento.
		 */
		public T getItem()
		{
			return elemento;
		}


	}

	//_________________________________________________________
	//constructor
	//_________________________________________________________

	/**
	 * crea una nueva cola
	 * se inicializan los atributos
	 * en valores inciales default
	 */
	public Queue() 
	{
		cabeza= null;
	}


	//_________________________________________________________
	//Metodos
	//_________________________________________________________
	
	/**
	 * retorna si una cola est� vac�a.
	 * @return true si esta vacia.
	 */
	public boolean isEmpty() 
	{
		boolean respuesta = false;
		if(cabeza == null)
		{
			respuesta = true;
		}
		return respuesta;
	}

	/**
	 * encola un nuevo elemento como 
	 * el �timo elemento de la cola.
	 * @param pElemento
	 */
	public void enqueue(T pElemento) 
	{
		Node aAgregar = new Node(pElemento);
		if(cabeza == null)
		{
			cabeza= aAgregar;
		}
		else
		{
			Node iterador = cabeza;
			boolean termino = false;
			while(!termino)
			{
				if(iterador.siguiente ==  null)
				{
					iterador.siguiente = aAgregar;
					termino = true;
					
				}
				else
				{
					iterador = iterador.siguiente;
				}
				
			}
		}
	}

	/**
	 * Retorna y elimina el primer elemento de la cola 
	 * encola al siguiente item como el �tlimo item.
	 * @return �ltimo elemento agregado en la pila.
	 */
	public T dequeue() 
	{
		T retorno = null;
		if(cabeza == null)
		{
			return null;
		}
		else
		{
			retorno = cabeza.elemento;
			cabeza = cabeza.siguiente;
			return retorno;
		}
	}

}
