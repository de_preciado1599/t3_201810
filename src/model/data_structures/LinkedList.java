package model.data_structures;

import model.data_structures.Lista.Node;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the first element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T> 
 */
public interface LinkedList <T extends Comparable<T>>  {

	//---------------------------------------------------------------------------------
	//METHODS 
	//---------------------------------------------------------------------------------

	public void add( T pElement);
	
	public void delete(T pElement);

	public T get(T pElement);
	
	public int size();
	
	public T get(int pPosition);

	public Node Listing();

	public T getCurrent();

	public T next();
}
