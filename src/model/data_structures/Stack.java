package model.data_structures;

public class Stack<T extends Comparable<T>>  implements IStack<T>{


	/**
	 * Representa el primer nodo de la pila.
	 */
	private Node first; 


	/**
	 * crea una nueva pila size=O && first = null.
	 */
	public Stack() 
	{
		// TODO Auto-generated constructor stub
		first= null;
	}



	/**
	 * Enlista un nuevo elemento como el �timo elemento de la pila.
	 */
	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		Node actual = new Node(t);
		if(first==null)
		{
			first= actual;
		}
		else
		{
			Node oldFirst = first;
			first = actual;
			actual.next = oldFirst;
		}
	}

	/**
	 * Retorna y elimina el �tlimo elemento de la pila y encola al antepen�ltimo item como el �tlimo item.
	 * @return �ltimo elemento agregado en la pila.
	 */
	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if(first== null){
			return null;
		}
		else{

			T itemActual = first.item;
			first = first.next;
			return itemActual;
		}
	}

	/**
	 * Consulta si una lista est� vac�a.
	 * @return false en caso de que la pila est� vac�a, true en caso contrario.
	 */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(first==null)
			return true;
		else 
			return false;
	}


	/**
	 * Es la clase que reperesenta un Nodo.
	 * @author sebas
	 *
	 */
	private class Node {

		/**
		 * Representa el Elemento actual de un nodo.
		 */
		private T item;

		/**
		 * Representa el siguiende Nodo de un Nodo actual.
		 */
		private Node next;

		public Node(T t){
			item = t;
		}
                                                                                                                                                                                                                                        
		/**
		 * Da el elemento actual sobre el nodo en el cual se encuentra.
		 * @return elemento actual de un nodo.
		 */
		public T getItem(){
			return item;
		}


	}
}
