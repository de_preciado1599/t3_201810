package model.data_structures;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase {
	
	//______________________________________________
	// atributos
	//______________________________________________
	private Queue <String> listaNombres;
	
	//______________________________________________
	//Setup escenarios ()
	//______________________________________________
	
	/**
	 * escenario 1 que tiene una cola
	 * con varios nombres
	 */
	private void setupEscenario1()
	{
		listaNombres = new Queue<String>();
		listaNombres.enqueue("daniel");
		listaNombres.enqueue("juan");
		listaNombres.enqueue("michael");
		listaNombres.enqueue("sebastian");

	}
	
	private void setupEscenario2()
	{
		listaNombres = new Queue<String>();
	}
	
	//______________________________________________
	//metodos que verifican los metodos de la cola
	//______________________________________________
	
	/**
	 * revisa si la cola esta vacia
	 * para ello se utiliza el escenario 2
	 */
	public void testIsEmpty()
	{
		setupEscenario2();
		assertEquals(true, listaNombres.isEmpty());
	}
	
	/**
	 * revisa si la cola no esta vacia
	 * para ello se utiliza el escenario 1
	 */
	public void testIsNotEmpty()
	{
		setupEscenario1();
		assertEquals(false, listaNombres.isEmpty());
	}
	
	
	/**
	 * revisa si la cola esta desencolando
	 * correctamente para ello se utiliza el escenario uno
	 * y se desencola hasta el ultimo elemento
	 */
	public void testDequeue()
	{
		setupEscenario1();
		assertEquals("daniel" , listaNombres.dequeue());
		assertEquals("juan" , listaNombres.dequeue());
		assertEquals("michael" , listaNombres.dequeue());
		assertEquals("sebastian" , listaNombres.dequeue());
		assertEquals( null , listaNombres.dequeue());
	}
	

}
