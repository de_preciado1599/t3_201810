package model.data_structures;
import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase{

	private Stack <String> nombres;
	
	private void setupEscenario1(){
		nombres = new Stack<String>();
		nombres.push("sebas");
		nombres.push("andres");
		nombres.push("ana mar�a");
		nombres.push("fernanda");
		nombres.push("carlos");
		nombres.push("manuel");

	}
	
	private void setupEscenario2(){
		nombres = new Stack<String>();
	}
	
	public void testPop(){
		setupEscenario1();
		assertEquals("manuel" , nombres.pop());
		assertEquals("carlos" , nombres.pop());
		assertEquals("fernanda" , nombres.pop());
		assertEquals("ana mar�a" , nombres.pop());
		assertEquals("andres" , nombres.pop());
		assertEquals("sebas" , nombres.pop());
		assertEquals( null , nombres.pop());
	}
	
	public void testIsEmpty(){
		setupEscenario2();
		assertEquals(true, nombres.isEmpty());
	}
	
	public void testIsNotEmpty(){
		setupEscenario1();
		assertEquals(false, nombres.isEmpty());
	}
	
	
}
